# Bundles utilizados:

Sonata::Admin (y sus dependencias):
composer require sonata-project/admin-bundle

Sonata::EasyExtends:
composer require sonata-project/easy-extends-bundle

Sonata::Media:
composer require sonata-project/media-bundle
 
FOS::User:
composer require friendsofsymfony/user-bundle "~1.3"

Doctrine::dbal:
composer require doctrine/dbal

Doctrine::DataFixtures:
composer require doctrine/data-fixtures