<?php

namespace AberturaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Aberturas
 *
 * @ORM\Table(name="aberturas")
 * @ORM\Entity(repositoryClass="AberturaBundle\Repository\AberturaRepository")
 * @UniqueEntity(fields = {"marca", "modelo"})
 */
class Abertura
{

    /**
     * @ORM\OneToMany(targetEntity="CarpinteriaBundle\Entity\Carpinteria", mappedBy="abertura")
     */
    private $carpinterias;

    /**
     * @ORM\ManyToOne(targetEntity="MarcasBundle\Entity\Marcas", inversedBy="aberturas")
     * @ORM\JoinColumn(name="marca_id", referencedColumnName="id")
     */
    private $marca;

    /**
     * @ORM\ManyToOne(targetEntity="ModeloBundle\Entity\Modelo", inversedBy="aberturas")
     * @ORM\JoinColumn(name="modelo_id", referencedColumnName="id")
     */
    private $modelo;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_metro_carpinteria", type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $valorMetroCarpinteria;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_metro_contramarco", type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $valorMetroContramarco;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_metro_premarco", type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $valorMetroPremarco;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Abertura
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valorMetroCarpinteria
     *
     * @param float $valorMetroCarpinteria
     *
     * @return Abertura
     */
    public function setValorMetroCarpinteria($valorMetroCarpinteria)
    {
        $this->valorMetroCarpinteria = $valorMetroCarpinteria;

        return $this;
    }

    /**
     * Get valorMetroCarpinteria
     *
     * @return float
     */
    public function getValorMetroCarpinteria()
    {
        return $this->valorMetroCarpinteria;
    }

    /**
     * Set valorMetroContramarco
     *
     * @param float $valorMetroContramarco
     *
     * @return Abertura
     */
    public function setValorMetroContramarco($valorMetroContramarco)
    {
        $this->valorMetroContramarco = $valorMetroContramarco;

        return $this;
    }

    /**
     * Get valorMetroContramarco
     *
     * @return float
     */
    public function getValorMetroContramarco()
    {
        return $this->valorMetroContramarco;
    }

    /**
     * Set valorMetroPremarco
     *
     * @param float $valorMetroPremarco
     *
     * @return Abertura
     */
    public function setValorMetroPremarco($valorMetroPremarco)
    {
        $this->valorMetroPremarco = $valorMetroPremarco;

        return $this;
    }

    /**
     * Get valorMetroPremarco
     *
     * @return float
     */
    public function getValorMetroPremarco()
    {
        return $this->valorMetroPremarco;
    }

    public function __construct()
    {
        $this->enabled = true;
    }


    /**
     * Set marca
     *
     * @param \MarcasBundle\Entity\Marcas $marca
     *
     * @return Abertura
     */
    public function setMarca(\MarcasBundle\Entity\Marcas $marca = null)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return \MarcasBundle\Entity\Marcas
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Set modelo
     *
     * @param \ModeloBundle\Entity\Modelo $modelo
     *
     * @return Abertura
     */
    public function setModelo(\ModeloBundle\Entity\Modelo $modelo = null)
    {
        $this->modelo = $modelo;

        return $this;
    }

    /**
     * Get modelo
     *
     * @return \ModeloBundle\Entity\Modelo
     */
    public function getModelo()
    {
        return $this->modelo;
    }

    public function __toString() {
        $modelo = $this->getModelo()?:'n/a';
        $marca  = $this->getMarca()?:'n/a';
        $modeloymarca = $modelo.', '.$marca;
        return $modeloymarca?:'n/a';
    }


    /**
     * Add carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     *
     * @return Abertura
     */
    public function addCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias[] = $carpinteria;

        return $this;
    }

    /**
     * Remove carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     */
    public function removeCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias->removeElement($carpinteria);
    }

    /**
     * Get carpinterias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarpinterias()
    {
        return $this->carpinterias;
    }
}
