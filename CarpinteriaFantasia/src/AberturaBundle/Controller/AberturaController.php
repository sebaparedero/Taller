<?php

namespace AberturaBundle\Controller;

use AberturaBundle\Entity\Abertura;
use AberturaBundle\Form\AberturaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AberturaController extends Controller
{

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $aberturas = $em->getRepository('AberturaBundle:Abertura')->findAll();
        return $this->render('AberturaBundle:Abertura:listar.html.twig', array('aberturas' => $aberturas));

    }

    public function unlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entidades = $em->getRepository('AberturaBundle:Abertura')->findEliminados();
        return $this->render('AberturaBundle:Abertura:unlisted.html.twig', array(
            'aberturas' => $entidades
        ));
    }

    public function addAction()
    {
        $entidad = new Abertura();
        $form = $this->createCreateForm($entidad);
        $em = $this->getDoctrine()->getManager();
        $marcas = $em->getRepository('MarcasBundle:Marcas')->findAll();
        $modelos = $em->getRepository('ModeloBundle:Modelo')->findAll();
        return $this->render('AberturaBundle:Abertura:form.html.twig', array(
            'form' => $form->createView(),
            'marcas' => $marcas,
            'modelos' => $modelos,
            'tituloForm' => 'Agregar Abertura',
        ));
    }

    private function createCreateForm(Abertura $entity)
    {
        $form = $this->createForm(new AberturaType(), $entity, array(
            'action' => $this->generateUrl('abertura_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $entidad = new Abertura();
        $form = $this->createCreateForm($entidad);
        $form->handleRequest($request);
        $form->getErrors(true);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entidad);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "La abertura se agrego exitosamente");
            return $this->redirectToRoute('abertura_add');
        } else {
            $this->addFlash('mensajeError', 'No se pudo agregar la abertura '.$form->getErrors());
        }

        return $this->render('AberturaBundle:Abertura:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AberturaBundle:Abertura')->find($id);
        if (!$entidad)
        {
            throw $this->createNotFoundException('No se encontró la abertura con id: '.$id);
        }

        $marcas = $em->getRepository('MarcasBundle:Marcas')->findAll();
        $modelos = $em->getRepository('ModeloBundle:Modelo')->findAll();
        $editForm = $this->createForm(AberturaType::class, $entidad);
        $editForm->handleRequest($request);
        $em->persist($entidad);
        $em->flush();

        return $this->render('AberturaBundle:Abertura:form.html.twig', array(
            'form' => $editForm->createView(),
            'marcas' => $marcas,
            'modelos' => $modelos,
            'id' => $id,
            'tituloForm' => 'Editar Abertura',
        ));
    }

    public function activarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AberturaBundle:Abertura')->find($id);
        $entidad->setEnabled(1);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('abertura_unlist');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AberturaBundle:Abertura')->find($id);
        $entidad->setEnabled(0);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('abertura_listar');
    }

    public function deleteFuerzaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AberturaBundle:Abertura')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('abertura_unlist');
    }


}