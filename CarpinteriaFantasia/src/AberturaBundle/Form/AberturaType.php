<?php

namespace AberturaBundle\Form;

use AberturaBundle\Entity\Abertura;
use AberturaBundle\Repository\AberturaRepository;
use MarcasBundle\Repository\MarcasRepository;
use ModeloBundle\Repository\ModeloRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AberturaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('valorMetroCarpinteria', NumberType::class)
            ->add('valorMetroContramarco', NumberType::class)
            ->add('valorMetroPremarco', NumberType::class)
            ->add('modelo', EntityType::class, array(
                'class' => 'ModeloBundle:Modelo',
                'query_builder' => function (ModeloRepository $mdr) {
                    return $mdr->findAllForm();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => true,
                'choice_label' => 'nombre',
            ))
            ->add('marca', EntityType::class, array(
                'class' => 'MarcasBundle:Marcas',
                'query_builder' => function (MarcasRepository $mcr) {
                    return $mcr->findAllForm();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => true,
                'choice_label' => 'nombre',
            ))
            ->add('enabled', null, array(
                'required' => false,
                'label' => 'Activado'
            ))
            ->add('save', 'submit', array('label' => 'Guardar Abertura'))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Abertura::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'abertura';
    }


}
