<?php
namespace AberturaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AberturaAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper

            ->with('Informacion Basica', array('class' => 'col-md-7'))
                ->add('modelo', 'sonata_type_model', array(
                    'class' => 'ModeloBundle\Entity\Modelo',
                    'property' => 'nombre',
                ))
                ->add('marca', 'sonata_type_model', array(
                    'class' => 'MarcasBundle\Entity\Marcas',
                    'property' => 'nombre',
                ))
            ->end()

            ->with('Valor', array('class' => 'col-md-5'))
                ->add('valorMetroCarpinteria', 'money')
                ->add('valorMetroContramarco', 'money')
                ->add('valorMetroPremarco', 'money')
            ->end()

            //->add('enabled', 'boolean')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('modelo', null, array(), 'entity', array(
                'class'    => 'ModeloBundle\Entity\Modelo',
                'property' => 'nombre',
            ))
            ->add('marca', null, array(), 'entity', array(
                'class'    => 'MarcasBundle\Entity\Marcas',
                'property' => 'nombre',
            ))
            //->add('valorMetroCarpinteria')
            //->add('valorMetroContramarco')
            //->add('valorMetroPremarco')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('modelo.nombre')
            ->addIdentifier('marca.nombre')
            ->add('valorMetroCarpinteria')
            ->add('valorMetroContramarco')
            ->add('valorMetroPremarco')
        ;
    }
}