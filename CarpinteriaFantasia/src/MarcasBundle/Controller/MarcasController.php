<?php

namespace MarcasBundle\Controller;

use MarcasBundle\Entity\Marcas;
use MarcasBundle\Form\MarcasType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MarcasController extends Controller
{

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $marcas = $em->getRepository('MarcasBundle:Marcas')->findAll();
        return $this->render('MarcasBundle:Marcas:listar.html.twig', array('marcas' => $marcas));
    }

    public function unlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entidades = $em->getRepository('MarcasBundle:Marcas')->findEliminados();
        return $this->render('MarcasBundle:Marcas:unlisted.html.twig', array(
            'marcas' => $entidades
        ));
    }

    public function addAction()
    {
        $marca = new Marcas();
        $form = $this->createCreateForm($marca);
        return $this->render('MarcasBundle:Marcas:form.html.twig', array(
            'form' => $form->createView(),
            'tituloForm' => 'Agregar Marca',
            ));
    }

    private function createCreateForm(Marcas $entity)
    {
        $form = $this->createForm(new MarcasType(), $entity, array(
            'action' => $this->generateUrl('marcas_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $marca = new Marcas();
        $form = $this->createCreateForm($marca);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($marca);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "La marca se agrego exitosamente");
            return $this->redirectToRoute('marcas_add');
        }

        return $this->render('MarcasBundle:Marcas:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('MarcasBundle:Marcas')->find($id);
        if (!$entidad)
        {
            throw $this->createNotFoundException('No se encontró la marca con id: '.$id);
        }

        $editForm = $this->createForm(MarcasType::class, $entidad);
        $editForm->handleRequest($request);
        $em->persist($entidad);
        $em->flush();

        return $this->render('MarcasBundle:Marcas:form.html.twig', array(
            'form' => $editForm->createView(),
            'tituloForm' => 'Editar Marca',
        ));
    }

    public function activarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('MarcasBundle:Marcas')->find($id);
        $entidad->setEnabled(1);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('marcas_unlist');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('MarcasBundle:Marcas')->find($id);
        $entidad->setEnabled(0);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('marcas_listar');
    }

    public function deleteFuerzaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('MarcasBundle:Marcas')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('marcas_unlist');
    }
}