<?php

namespace MarcasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Marcas
 *
 * @ORM\Table(name="marcas")
 * @ORM\Entity(repositoryClass="MarcasBundle\Repository\MarcasRepository")
 * @UniqueEntity("nombre")
 */
class Marcas
{

    /**
     * @ORM\OneToMany(targetEntity="AberturaBundle\Entity\Abertura", mappedBy="marca")
     */
    private $aberturas;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60, unique=true)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El nombre tiene que tener al menos 2 caracteres",
     *      maxMessage = "El nombre no puede tener mas de 60 caracteres"
     * )
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Marcas
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Marcas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __construct()
    {
        $this->enabled = true;
        $this->aberturas = new ArrayCollection();
    }


    /**
     * Add abertura
     *
     * @param \AberturaBundle\Entity\Abertura $abertura
     *
     * @return Marcas
     */
    public function addAbertura(\AberturaBundle\Entity\Abertura $abertura)
    {
        $this->aberturas[] = $abertura;

        return $this;
    }

    /**
     * Remove abertura
     *
     * @param \AberturaBundle\Entity\Abertura $abertura
     */
    public function removeAbertura(\AberturaBundle\Entity\Abertura $abertura)
    {
        $this->aberturas->removeElement($abertura);
    }

    /**
     * Get aberturas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAberturas()
    {
        return $this->aberturas;
    }

    public function __toString() {
        return $this->nombre?:'n/a';
    }

}
