<?php

namespace PresupuestoBundle\Form;

use Doctrine\ORM\EntityRepository;
use PresupuestoBundle\Entity\Presupuesto;
use ClienteBundle\Repository\ClienteRepository;
use Symfony\Component\Form\AbstractType;
use CarpinteriaBundle\Form\CarpinteriaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PresupuestoType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('carpinterias', CollectionType::class, array(
                'entry_type' => CarpinteriaType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
            ))
            ->add('costoColocacion', 'number', array(
                'required' => false,
                'label' => 'Costo de colocación'
            ))
            ->add('costoEnvio', 'number', array(
                'required' => false,
                'label' => 'Costo de envío'
            ))
            ->add('costoTotal','number', array('disabled' => true))
            ->add('enabled')
            ->add('user', null, array(
                'label' => 'Usuario',
                'disabled' => true
            ))
            ->add('cliente', EntityType::class, array(
                'class' => 'ClienteBundle:Cliente',
                'query_builder' => function (ClienteRepository $cr) {
                    return $cr->findAllForm();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => true,
            ))
            ->add('save', 'submit', array('label' => 'Agregar Presupuesto'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Presupuesto::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'presupuesto';
    }


}
