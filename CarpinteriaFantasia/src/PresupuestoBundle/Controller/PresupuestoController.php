<?php

namespace PresupuestoBundle\Controller;

use CarpinteriaBundle\Controller\CarpinteriaController;
use CarpinteriaBundle\Entity\Carpinteria;
use ModeloBundle\Entity\Modelo;
use ModeloBundle\Form\ModeloType;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use PresupuestoBundle\Entity\Presupuesto;
use PresupuestoBundle\Form\PresupuestoType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\Dompdf;
use Dompdf\Options;

class PresupuestoController extends Controller
{
    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $presupuestos = $em->getRepository('PresupuestoBundle:Presupuesto')->findAll();
        return $this->render('PresupuestoBundle:Presupuesto:listar.html.twig', array(
            'presupuestos' => $presupuestos
        ));
    }

    public function addAction()
    {
        $presupuesto = new Presupuesto();
        $form = $this->createCreateForm($presupuesto);
        return $this->render('PresupuestoBundle:Presupuesto:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    private function createCreateForm(Presupuesto $entity)
    {
        $form = $this->createForm(new PresupuestoType(), $entity, array(
            'action' => $this->generateUrl('presupuesto_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $presupuesto = new Presupuesto();
        $form = $this->createCreateForm($presupuesto);
        $form->handleRequest($request);
        $form->getErrors(true);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $usuario = $this->get('security.context')->getToken()->getUser();
            $presupuesto->setUser($usuario);
            $em->persist($presupuesto);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "El presupuesto se agrego exitosamente");
            return $this->redirectToRoute('presupuesto_edit', array('id' => $presupuesto->getId()));
        } else {
            $this->addFlash('mensajeError', 'No se pudo agregar el presupuesto '.$form->getErrors());
        }

        return $this->render('PresupuestoBundle:Presupuesto:add.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id);
        if (!$presupuesto)
        {
            throw $this->createNotFoundException('No se encontró el presupuesto con id: '.$id);
        } else {
            $carpinterias = $presupuesto->getCarpinterias();
        }

        $editForm = $this->createForm(PresupuestoType::class, $presupuesto);
        $editForm->handleRequest($request);
        $em->persist($presupuesto);
        $em->flush();

        return $this->render('PresupuestoBundle:Presupuesto:listarCarpinteriasPresupuesto.html.twig', array(
                'form' => $editForm->createView(),
                'carpinterias' => $carpinterias,
                'id' => $id
        ));
    }

    public function addCarpinteriaAction($id)
    {
        return $this->redirectToRoute('carpinteria_add', array('id' => $id));
    }

    public function deleteAction($id)
    {
       $em = $this->getDoctrine()->getManager();
       $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id);
       $em->remove($presupuesto);
       $em->flush();
       return $this->redirectToRoute('presupuesto_listar');
    }

    /**
     * @Route("/deleteAjax")
     */
    public function deleteAjaxAction()
    {
        $request = $this->container->get('request');
        $id_presupuesto = $request->request->get('id_presupuesto');
        $em = $this->getDoctrine()->getManager();
        $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id_presupuesto);
        $em = $this->getDoctrine()->getManager();
        $em->remove($presupuesto);
        $em->flush();
        return $this->redirectToRoute('presupuesto_listar');
    }

    public function pdfAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id);
        if (!$presupuesto)
        {
            throw $this->createNotFoundException('No se encontró el presupuesto con id: '.$id);
        }
        $carpinterias = $presupuesto->getCarpinterias();
        $html = $this->renderView('PresupuestoBundle:Presupuesto:template.html.twig', array(
            'presupuesto' => $presupuesto,
            'carpinterias' => $carpinterias,
            'id' => $id,
            'title' =>  'Fantasia S.A',
            'title2' => 'Presupuesto'
        ));

        $options = new Options();
        $options->set('isRemoteEnabled', true);

        $dompdf = new Dompdf();
        $dompdf->setOptions($options);
        //$dompdf->set_option('isHtml5ParserEnabled', true);
        $dompdf->loadHtml($html);
        $dompdf->output();
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));

        exit(0);

    }

    public function pdfBAction($id)
    {
        //$snappy = $this->get('knp_snappy.pdf');
        //$snappy->setOption('no-outline', true);
        //$snappy->setOption('page-size','a4');
        //$snappy->setOption('encoding', 'UTF-8');
        $pdfGenerator = $this->get('spraed.pdf.generator');

        $em = $this->getDoctrine()->getManager();
        $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id);
        if (!$presupuesto)
        {
            throw $this->createNotFoundException('No se encontró el presupuesto con id: '.$id);
        }
        $carpinterias = $presupuesto->getCarpinterias();

        $html = $this->renderView('PresupuestoBundle:Presupuesto:template.html.twig', array(
            'presupuesto' => $presupuesto,
            'carpinterias' => $carpinterias,
            'id' => $id,
            'title' =>  'Fantasia S.A',
            'title2' => 'Presupuesto'
        ));

        #$filename = 'Presupuesto';

        return new Response($pdfGenerator->generatePDF($html),
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename=out.pdf'
            )
        );

    }


}