<?php

namespace PresupuestoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * Presupuestos
 *
 * @ORM\Table(name="presupuestos")
 * @ORM\Entity(repositoryClass="PresupuestoBundle\Repository\PresupuestoRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Presupuesto
{

    /**
     * @ORM\OneToMany(targetEntity="CarpinteriaBundle\Entity\Carpinteria", mappedBy="presupuesto", cascade={"remove"})
     */
    private $carpinterias;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="presupuestos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="ClienteBundle\Entity\Cliente", inversedBy="presupuestos")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $cliente;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     * @Assert\NotBlank()
     * @Assert\Date
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="costoColocacion", type="float", nullable=true)
     * @Assert\Type("float")
     */
    private $costoColocacion;

    /**
     * @var float
     *
     * @ORM\Column(name="costoEnvio", type="float", nullable=true)
     * @Assert\Type("float")
     */
    private $costoEnvio;

    /**
     * @var float
     *
     * @ORM\Column(name="costoCarpinterias", type="float", nullable=true)
     * @Assert\Type("float")
     */
    private $costoCarpinterias;

    /**
     * @var float
     *
     * @ORM\Column(name="costoTotal", type="float", nullable=true)
     * @Assert\Type("float")
     */
    private $costoTotal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Presupuesto
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cliente
     *
     * @param \ClienteBundle\Entity\Cliente $cliente
     *
     * @return Presupuesto
     */
    public function setCliente(\ClienteBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \ClienteBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->carpinterias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setCostoTotal(0.00);
        $this->setFecha(new \DateTime('now'));
        //$this->setFecha();
        $this->enabled = true;
    }

    /**
     * Add carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     *
     * @return Presupuesto
     */
    public function addCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias[] = $carpinteria;

        return $this;
    }

    /**
     * Remove carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     */
    public function removeCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias->removeElement($carpinteria);
    }

    /**
     * Get carpinterias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarpinterias()
    {
        return $this->carpinterias;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Presupuesto
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function __toString() {
        $user = $this->getUser()->getNombre().' '.$this->getUser()->getApellido();
        $cliente = $this->getCliente()->getNombre().' '.$this->getCliente()->getApellido();
        $presupuestoCompleto = $user.', '.$cliente;
        return $presupuestoCompleto?:'n/a';
    }


    /**
     * Set costoColocacion
     *
     * @param float $costoColocacion
     *
     * @return Presupuesto
     */
    public function setCostoColocacion($costoColocacion)
    {
        $this->costoColocacion = $costoColocacion;

        return $this;
    }

    /**
     * Get costoColocacion
     *
     * @return float
     */
    public function getCostoColocacion()
    {
        return $this->costoColocacion;
    }

    /**
     * Set costoEnvio
     *
     * @param float $costoEnvio
     *
     * @return Presupuesto
     */
    public function setCostoEnvio($costoEnvio)
    {
        $this->costoEnvio = $costoEnvio;

        return $this;
    }

    /**
     * Get costoEnvio
     *
     * @return float
     */
    public function getCostoEnvio()
    {
        return $this->costoEnvio;
    }

    /**
     * Set costoTotal
     *
     * @param float $costoTotal
     *
     * @return Presupuesto
     */
    public function setCostoTotal($costoTotal)
    {
        $this->costoTotal = $costoTotal;

        return $this;
    }

    /**
     * Get costoTotal
     *
     * @return float
     */
    public function getCostoTotal()
    {
        return $this->costoTotal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Presupuesto
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @ORM\PostLoad
     */
    public function calcularTotal()
    {
        $costosBasicos = $this->getCostoEnvio() + $this->getCostoColocacion();
        $totalCarpinterias = $this->calcularCostoCarpinterias();
        $total = $costosBasicos + $totalCarpinterias;
        $this->setCostoCarpinterias($totalCarpinterias);
        $this->setCostoTotal($total);
    }

    /**
     * Set costoCarpinterias
     *
     * @param float $costoCarpinterias
     *
     * @return Presupuesto
     */
    public function setCostoCarpinterias($costoCarpinterias)
    {
        $this->costoCarpinterias = $costoCarpinterias;

        return $this;
    }

    /**
     * Get costoCarpinterias
     *
     * @return float
     */
    public function getCostoCarpinterias()
    {
        return $this->costoCarpinterias;
    }

    public function calcularCostoCarpinterias()
    {
        $carpinterias = $this->getCarpinterias();
        $totalCarpinterias = 0;
        foreach($carpinterias as $carpinteria) {
            $totalCarpinterias = $totalCarpinterias + $carpinteria->getCosto();
        }
        return $totalCarpinterias;
    }
}
