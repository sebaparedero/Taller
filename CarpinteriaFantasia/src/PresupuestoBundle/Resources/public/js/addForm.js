var $collectionHolder;

// Añade boton al final de las carpinterias, para agregar mas dinámicamente
var $addCarpinteriaLink = $('<a href="#" class="add_carpinteria_link btn btn-default">Añadir carpinteria</a>');
var $newLinkLi = $('<li></li>').append($addCarpinteriaLink);

jQuery(document).ready(function() {
    // Get ul con info de colección de carpinterias
    $collectionHolder = $('ul.carpinterias');

    // add the "add a carpinteria" anchor and li to the carpinterias ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addCarpinteriaLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new carpinteria form (see next code block)
        addCarpinteriaForm($collectionHolder, $newLinkLi);
    });

});

function addCarpinteriaForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your tags field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<div class="col-md-4"></div>').append(newForm);
    $newLinkLi.before($newFormLi);
}