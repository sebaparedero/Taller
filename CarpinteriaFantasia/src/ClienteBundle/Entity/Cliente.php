<?php

namespace ClienteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cliente
 *
 * @ORM\Table(name="clientes")
 * @ORM\Entity(repositoryClass="ClienteBundle\Repository\ClienteRepository")
 */
class Cliente
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="PresupuestoBundle\Entity\Presupuesto", mappedBy="cliente")
     */
    private $presupuestos;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El nombre tiene que tener al menos 2 caracteres",
     *      maxMessage = "El nombre no puede tener mas de 60 caracteres"
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=60)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El apellido tiene que tener al menos 2 caracteres",
     *      maxMessage = "El apellido no puede tener mas de 60 caracteres"
     * )
     */
    private $apellido;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(name="telefono", type="string", nullable=true, length=13)
     * @Assert\Regex(
     *     pattern="/^[0-9]+$/i",
     *     message="Número inválido"
     * )
     * @Assert\Length(
     *      min = 9,
     *      max = 13,
     *      minMessage = "Número inválido",
     *      maxMessage = "Número inválido"
     * )
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=60, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El domicilio tiene que tener al menos 2 caracteres",
     *      maxMessage = "El domicilio no puede tener mas de 60 caracteres"
     * )
     */
    private $domicilio;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=80, nullable=true, unique=true)
     * @Assert\Email(
     *     message = "El email '{{ value }}' no es válido.",
     *     checkMX = true
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     * @Assert\Length(
     *      max = 20000,
     *      maxMessage = "Las observaciones son de máximo 20000 caracteres"
     * )
     */
    private $observaciones;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Cliente
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return Cliente
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set telefono
     *
     * @param integer $telefono
     *
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return int
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     *
     * @return Cliente
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     *
     * @return Cliente
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    public function __construct()
    {
        $this->enabled = true;
        $this->presupuestos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add presupuesto
     *
     * @param \PresupuestoBundle\Entity\Presupuesto $presupuesto
     *
     * @return Cliente
     */
    public function addPresupuesto(\PresupuestoBundle\Entity\Presupuesto $presupuesto)
    {
        $this->presupuestos[] = $presupuesto;

        return $this;
    }

    /**
     * Remove presupuesto
     *
     * @param \PresupuestoBundle\Entity\Presupuesto $presupuesto
     */
    public function removePresupuesto(\PresupuestoBundle\Entity\Presupuesto $presupuesto)
    {
        $this->presupuestos->removeElement($presupuesto);
    }

    /**
     * Get presupuestos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresupuestos()
    {
        return $this->presupuestos;
    }

    public function __toString() {
        $nombreyapellido = $this->nombre.' '.$this->apellido;
        return $nombreyapellido;
    }

}
