<?php
namespace ClienteBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ClienteAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Informacion de Contacto', array('class' => 'col-md-4'))
                ->add('nombre', 'text')
                ->add('apellido', 'text')
                ->add('telefono', 'integer', array('required' => false))
                ->add('domicilio', 'text', array('required' => false))
                ->add('email', 'text', array('required' => false))
            ->end()
            ->with('Observaciones', array('class' => 'col-md-8'))
                ->add('observaciones', 'textarea', array('required' => false))
                //->add('enabled', 'boolean')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('apellido')
            ->add('email')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('nombre')
            ->addIdentifier('apellido')
            ->add('telefono')
            ->add('domicilio')
            ->add('email')
        ;
    }
}