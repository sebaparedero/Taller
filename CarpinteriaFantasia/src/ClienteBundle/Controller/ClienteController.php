<?php

namespace ClienteBundle\Controller;

use ClienteBundle\Entity\Cliente;
use ClienteBundle\Form\ClienteType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ClienteController extends Controller
{
    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('ClienteBundle:Cliente')->findAll();
        return $this->render('ClienteBundle:Cliente:listar.html.twig', array('clientes' => $clientes));
    }

    public function unlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository('ClienteBundle:Cliente')->findEliminados();
        return $this->render('ClienteBundle:Cliente:unlisted.html.twig', array(
            'clientes' => $clientes
        ));
    }

    public function addAction()
    {
        $entidad = new Cliente();
        $form = $this->createCreateForm($entidad);
        return $this->render('ClienteBundle:Cliente:form.html.twig', array(
            'form' => $form->createView(),
            'tituloForm' => 'Agregar Cliente'
        ));
    }

    private function createCreateForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('cliente_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $entidad = new Cliente();
        $form = $this->createCreateForm($entidad);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entidad);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "El cliente se agrego exitosamente");
            return $this->redirectToRoute('cliente_add');
        }

        return $this->render('ClienteBundle:Cliente:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ClienteBundle:Cliente')->find($id);
        if (!$entidad)
        {
            throw $this->createNotFoundException('No se encontró el cliente con id: '.$id);
        }

        $editForm = $this->createForm(ClienteType::class, $entidad);
        $editForm->handleRequest($request);
        $em->persist($entidad);
        $em->flush();

        return $this->render('ClienteBundle:Cliente:form.html.twig', array(
            'form' => $editForm->createView(),
            'tituloForm' => 'Editar Cliente',
        ));
    }

    public function activarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ClienteBundle:Cliente')->find($id);
        $entidad->setEnabled(1);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('cliente_unlist');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ClienteBundle:Cliente')->find($id);
        $entidad->setEnabled(0);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('cliente_listar');
    }

    public function deleteFuerzaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ClienteBundle:Cliente')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('cliente_unlist');
    }

}