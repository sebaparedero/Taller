<?php

namespace VidrioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Vidrio
 *
 * @ORM\Table(name="vidrios")
 * @ORM\Entity(repositoryClass="VidrioBundle\Repository\VidrioRepository")
 */
class Vidrio
{

    /**
     * @ORM\OneToMany(targetEntity="CarpinteriaBundle\Entity\Carpinteria", mappedBy="vidrio")
     */
    private $carpinterias;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 1,
     *      max = 50,
     *      minMessage = "La descripcion tiene que tener al menos 1 caracter",
     *      maxMessage = "La descripcion no puede tener mas de 50 caracteres"
     * )
     */
    private $descripcion;

    /**
     * @var int
     *
     * @ORM\Column(name="espesor", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $espesor;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_m2", type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $valorM2;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Vidrio
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Vidrio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set espesor
     *
     * @param integer $espesor
     *
     * @return Vidrio
     */
    public function setEspesor($espesor)
    {
        $this->espesor = $espesor;

        return $this;
    }

    /**
     * Get espesor
     *
     * @return int
     */
    public function getEspesor()
    {
        return $this->espesor;
    }

    /**
     * Set valorM2
     *
     * @param float $valorM2
     *
     * @return Vidrio
     */
    public function setValorM2($valorM2)
    {
        $this->valorM2 = $valorM2;

        return $this;
    }

    /**
     * Get valorM2
     *
     * @return float
     */
    public function getValorM2()
    {
        return $this->valorM2;
    }

    public function __construct()
    {
        $this->enabled = true;
        $this->carpinterias = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Add carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     *
     * @return Vidrio
     */
    public function addCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias[] = $carpinteria;

        return $this;
    }

    /**
     * Remove carpinteria
     *
     * @param \CarpinteriaBundle\Entity\Carpinteria $carpinteria
     */
    public function removeCarpinteria(\CarpinteriaBundle\Entity\Carpinteria $carpinteria)
    {
        $this->carpinterias->removeElement($carpinteria);
    }

    /**
     * Get carpinterias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCarpinterias()
    {
        return $this->carpinterias;
    }

    public function __toString() {

        return $this->getDescripcion().', '.$this->getEspesor().' mm.';
    }

    public function getFullName()
    {
        return $this->getDescripcion().', '.$this->getEspesor().' mm.';
    }

}
