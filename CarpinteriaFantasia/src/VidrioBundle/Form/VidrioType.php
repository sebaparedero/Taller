<?php

namespace VidrioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VidrioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion', 'text', array(
                'label' => 'Descripción',
                #'attr' => 'Descripción',
                #'placeholder' => 'Descripción',
            ))
            ->add('espesor')
            ->add('valorM2', 'number', array(
                'label' => 'Valor por metro cuadrado'
            ))
            ->add('enabled', null, array(
                'required' => false,
                'label' => 'Activado'
            ))
            ->add('save', 'submit', array('label' => 'Guardar Vidrio'))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'VidrioBundle\Entity\Vidrio'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'vidrio';
    }


}
