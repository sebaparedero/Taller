<?php

namespace VidrioBundle\Controller;

use VidrioBundle\Entity\Vidrio;
use VidrioBundle\Form\VidrioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class VidrioController extends Controller
{
    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $vidrios = $em->getRepository('VidrioBundle:Vidrio')->findAll();
        return $this->render('VidrioBundle:Vidrio:listar.html.twig', array('vidrios' => $vidrios));

    }

    public function unlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entidades = $em->getRepository('VidrioBundle:Vidrio')->findEliminados();
        return $this->render('VidrioBundle:Vidrio:unlisted.html.twig', array(
            'vidrios' => $entidades
        ));
    }

    public function addAction()
    {
        $entidad = new Vidrio();
        $form = $this->createCreateForm($entidad);
        return $this->render('VidrioBundle:Vidrio:form.html.twig', array(
            'form' => $form->createView(),
            'tituloForm' => 'Agregar Vidrio'
        ));
    }

    private function createCreateForm(Vidrio $entity)
    {
        $form = $this->createForm(new VidrioType(), $entity, array(
            'action' => $this->generateUrl('vidrio_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $entidad = new Vidrio();
        $form = $this->createCreateForm($entidad);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entidad);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "El vidrio se agrego exitosamente");
            return $this->redirectToRoute('vidrio_add');
        }

        return $this->render('VidrioBundle:Vidrio:form.html.twig', array('form' => $form->createView()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('VidrioBundle:Vidrio')->find($id);
        if (!$entidad)
        {
            throw $this->createNotFoundException('No se encontró el vidrio con id: '.$id);
        }

        $editForm = $this->createForm(VidrioType::class, $entidad);
        $editForm->handleRequest($request);
        $em->persist($entidad);
        $em->flush();

        return $this->render('VidrioBundle:Vidrio:form.html.twig', array(
            'form' => $editForm->createView(),
            'tituloForm' => 'Editar Vidrio',
            'id' => $id
        ));
    }

    public function activarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('VidrioBundle:Vidrio')->find($id);
        $entidad->setEnabled(1);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('vidrio_unlist');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('VidrioBundle:Vidrio')->find($id);
        $entidad->setEnabled(0);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('vidrio_listar');
    }

    public function deleteFuerzaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('VidrioBundle:Vidrio')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('vidrio_unlist');
    }

}