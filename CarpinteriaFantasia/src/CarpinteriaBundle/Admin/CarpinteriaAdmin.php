<?php
namespace CarpinteriaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CarpinteriaAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Informacion General', array('class' => 'col-md-6'))
                //->add('abertura', 'sonata_type_model', array(
                //    'class' => 'AberturaBundle\Entity\Abertura',
                //    'property' => 'nombre',
                //))
                ->add('vidrio', 'sonata_type_model', array(
                    'class' => 'VidrioBundle\Entity\Vidrio',
                ))
                //->add('presupuesto', 'sonata_type_model', array(
                //    'class' => 'PresupuestoBundle\Entity\Presupuesto',
                //    'property' => 'nombre',
                //))
            ->end()
            ->with('Precio y cantidad', array('class' => 'col-md-6'))
                ->add('cantidad', 'integer')
                ->add('anchoVidrio', 'number')
                ->add('altoVidrio', 'number')
                ->add('contramarco',
                    'choice', array(
                        'choices' => array(
                            'true' => 'Si',
                            'false' => 'No'
                        ))
                )
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('abertura')
            ->add('vidrio', null, array(), 'entity', array(
                'class'    => 'VidrioBundle\Entity\Vidrio',
                'property' => 'descripcion',
            ))
            ->add('cantidad')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            //->addIdentifier('abertura')
            ->addIdentifier('vidrio.descripcion')
            ->add('cantidad')
            ->add('anchoVidrio')
            ->add('altoVidrio')
            ->add('premarco')
            ->add('contramarco')
        ;
    }
}