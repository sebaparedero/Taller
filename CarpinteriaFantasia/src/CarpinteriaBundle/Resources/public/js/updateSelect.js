$(document).ready(function(){

    function calcularTotalAbertura(){
        var alto = ($('#carpinteria_altoVidrio').val()) / 1000;
        var ancho = ($('#carpinteria_anchoVidrio').val()) / 1000;
        var metros_lineales = (alto + ancho) * 2;
        var carpinteria = $('#valorCarpinteria').text();
        var premarco = $('#valorPremarco').text();
        var contramarco = 0;
        if ($('#carpinteria_contramarco').is(':checked')) {
            contramarco = $('#valorContramarco').text();
        }
        if (ancho && alto && carpinteria && premarco) {
            var valor_abertura_total = (((metros_lineales) * +carpinteria) +
                                        ((metros_lineales) * +premarco) +
                                        ((metros_lineales) * +contramarco));
            valor_abertura_total = (valor_abertura_total).toFixed(2);
            $('#valorAberturaTotal').empty();
            $('#valorAberturaTotal').show();
            $('#valorAberturaTotal').append($("<br>"));
            $('#valorAberturaTotal').append($("<label>Valor Total Abertura: &nbsp;</label>"));
            $('#valorAberturaTotal').append($("<label class='label label-success' id='totalAbertura'>"
                + valor_abertura_total + "</label>"));
            calcularTotal();
        } else {
            $('#valorAberturaTotal').empty();
            $('#valorAberturaTotal').hide();
        }
    }

    function calcularTotalVidrio(){
        var valor_vidrio_M2 = $('#valorM2Vidrio').text();
        var alto_vidrio = $('#carpinteria_altoVidrio').val() / 1000;
        var ancho_vidrio = $('#carpinteria_anchoVidrio').val() / 1000;
        //var path = $('#vidriopath').attr("data-path");
        if(alto_vidrio && ancho_vidrio && valor_vidrio_M2){
            var valor_vidrio_total = (+valor_vidrio_M2 * alto_vidrio * ancho_vidrio);
            valor_vidrio_total = (valor_vidrio_total).toFixed(2);
            $('#valorVidrioTotal').empty();
            $('#valorVidrioTotal').show();
            $('#valorVidrioTotal').append($("<label>Valor Total Vidrio: &nbsp;</label>"));
            $('#valorVidrioTotal').append($("<label class='label label-success' id='totalVidrio'>"+valor_vidrio_total+"</label>"));
            calcularTotal();
        }else{
            $('#valorVidrioTotal').empty();
            $('#valorVidiroTotal').hide();
            $('#valorTotal').empty();
            $('#valorTotal').hide();
        }
    }

    function generarSelectMarcas() {
        var modeloID = $('#carpinteria_modelo').val();
        var path = $('#modelo').attr("data-path");
        if(modeloID){
            $.ajax({
                type:'POST',
                url: path,
                data:{
                    id_modelo: +modeloID
                },
                dataType: 'JSON',
                success:function(marcas){
                    $('#divmarca').show();
                    $('#marca').empty();
                    $('#valoresAbertura').empty();
                    $('#valoresAbertura').hide();
                    $('#marca').append($("<option></option>").attr("value",'').text(''));
                    $.each(marcas, function(key, marca) {
                        $('#marca')
                            .append($("<option></option>")
                                .attr("value",marca.id)
                                .text(marca.nombre));
                    });

                    mostrarValores();

                }
            });
        }else{
            $('#divmarca').hide();
            $('#valoresAbertura').hide();
            $('#valorTotal').empty();
            $('#valorTotal').hide();
            $('#valorAberturaTotal').hide();
            $('#carpinteria_abertura').val(null);
        }
    }

    function mostrarValores() {
        var id_marca = $('#marca').val();
        var id_modelo = $('#carpinteria_modelo').val();
        var path = $('#marca').attr("data-path");
        if(id_marca && id_modelo){
            $.ajax({
                type:'POST',
                url: path,
                data:{
                    id_modelo: +id_modelo,
                    id_marca: +id_marca
                },
                dataType: 'JSON',
                success:function(aberturas){
                    $.each(aberturas, function(key, abertura) {
                        $('#valoresAbertura').empty();
                        $('#valoresAbertura').show();
                        $('#valoresAbertura').attr("value",abertura.id);

                        $('#valoresAbertura').append($("<label style='color: grey '>Valor Metro Carpinteria: &nbsp;</label>"));
                        $('#valoresAbertura')
                            .append($("<label class='label label-success' id='valorCarpinteria'></label>")
                                .text(abertura.valorMetroCarpinteria.toFixed(2)));
                        $('#valoresAbertura').append($("<br>"));

                        $('#valoresAbertura').append($("<label style='color: grey '>Valor Metro Contramarco: &nbsp;</label>"));
                        $('#valoresAbertura')
                            .append($("<label class='label label-success' id='valorContramarco'></label>")
                                .text(abertura.valorMetroContramarco.toFixed(2)));
                        $('#valoresAbertura').append($("<br>"));

                        $('#valoresAbertura').append($("<label style='color: grey '>Valor Metro Premarco: &nbsp;</label>"));
                        $('#valoresAbertura')
                            .append($("<label class='label label-success' id='valorPremarco'></label>")
                                .text(abertura.valorMetroPremarco.toFixed(2)));

                        $('#carpinteria_abertura').val(abertura.id);

                        calcularTotalAbertura();
                        //calcularTotalVidrio();
                        //calcularTotal();
                    });
                }
            });
        }else{
            $('#valorAberturaTotal').hide();
            $('#valorAberturaTotal').empty();
            $('#valoresAbertura').empty();
            $('#valoresAbertura').hide();
            $('#carpinteria_abertura').val(null);
            calcularTotal();
            //$('#marca').html('<option value="">Selecciona un modelo primero</option>');
        }
    }

    function calcularTotal() {
        var total_vidrio = $('#totalVidrio').text();
        var total_abertura = $('#totalAbertura').text();
        var cantidad = $('#carpinteria_cantidad').val();
        if (total_vidrio && total_abertura && cantidad){
            var totalTodo = ((+total_abertura + +total_vidrio) * cantidad);
            totalTodo = (totalTodo).toFixed(2);
            $('#valorTotal').empty();
            $('#valorTotal').show();
            $('#valorTotal').append($("" +
                "<h4>" +
                "   <label>Valor Total: &nbsp;</label>" +
                "</h4>"));
            $('#valorTotal').append($("" +
                "<h4>" +
                "   <label class='label label-success'>$</label>" +
                "   <label class='label label-success' id='totalTodo'>"+totalTodo+"</label>" +
                "</h4>"));
            $('#valorTotal').append($("<br>"));

        } else {
            $('#valorTotal').empty();
            $('#valorTotal').hide();
        }
    }

    function mostrarValoresVidrio() {
        var id_vidrio = $('#carpinteria_vidrio').val();
        var path = $('#vidriopath').attr("data-path");
        if(id_vidrio){
            $.ajax({
                type:'POST',
                url: path,
                data:{
                    id_vidrio: +id_vidrio
                },
                dataType: 'JSON',
                success:function(vidrios){
                    console.log(vidrios);
                    $.each(vidrios, function(key, vidrio) {
                        $('#valorVidrio').empty();
                        $('#valorVidrio').show();

                        $('#valorVidrio').append($("<label style='color: grey'>Valor Metro Cuadrado: &nbsp;</label>"));
                        $('#valorVidrio')
                            .append($("<label class='label label-success' id='valorM2Vidrio'></label>")
                                .text(vidrio.valorM2.toFixed(2)));
                        $('#valorVidrio').append($("<br>"));
                        $('#valorVidrio').append($("<br>"));

                        $('#carpinteria_vidrio').val(vidrio.id);
                        calcularTotalVidrio();

                    });
                }
            });
        } else {
            $('#valorVidrio').empty();
            $('#valorVidrioTotal').empty();
            $('#valorVidiro').hide();
            $('#valorVidrioTotal').hide();
            $('#carpinteria_vidrio').val(null);
        }
    }

    $('#carpinteria_modelo').on('change',function(){
        generarSelectMarcas();
        calcularTotalAbertura();
    });

    $('#marca').on('change',function(){
        mostrarValores();
        calcularTotal();
    });

    $('#carpinteria_vidrio').on('change',function(){
        mostrarValoresVidrio();
    });

    $('#carpinteria_altoVidrio, #carpinteria_anchoVidrio').on('change keypress keyup',function(){
        calcularTotalAbertura();
        calcularTotalVidrio();
        calcularTotal();
    });

    $('#carpinteria_cantidad').on('change keypress keyup',function(){
        calcularTotal();
    });

    $('#carpinteria_contramarco').on('change',function(){
        calcularTotalAbertura();
        calcularTotal();
    });

    // LO SIGUIENTE SE EJECUTA AUTOMATICAMENTE AL CARGAR LA PAGINA //

    // CHEQUEA SI ES EDICION //

        // GENERAR MARCAS
        var id_modelo_abertura = $('#abertura_modelo').val();
        if (id_modelo_abertura){
            $('#carpinteria_modelo').val(id_modelo_abertura);
            generarSelectMarcas();
        }

        // SETEAR MARCA
        var id_marca_abertura = $('#abertura_marca').val();
        if (id_marca_abertura) {
            $('#marca').val(id_marca_abertura);
        }

        // MOSTRAR VALOR VIDRIO
        var id_vidrio = $('#carpinteria_vidrio').val();
        if (id_vidrio) {
            mostrarValoresVidrio();
        }

});