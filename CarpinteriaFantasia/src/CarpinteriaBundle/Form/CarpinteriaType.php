<?php

namespace CarpinteriaBundle\Form;

use CarpinteriaBundle\Entity\Carpinteria;
use AberturaBundle\Entity\Abertura;
use VidrioBundle\Entity\Vidrio;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use VidrioBundle\Repository\VidrioRepository;
use ModeloBundle\Repository\ModeloRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class CarpinteriaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('abertura', null, array(
                'required' => false,
            ))
            ->add('modelo', EntityType::class, array(
                'class' => 'ModeloBundle:Modelo',
                'query_builder' => function (ModeloRepository $mr) {
                    return $mr->findModelosAberturas();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
            ))
            ->add('vidrio', EntityType::class, array(
                'class' => 'VidrioBundle:Vidrio',
                'query_builder' => function (VidrioRepository $vr) {
                    return $vr->findAllForm();
                },
                'expanded' => false,
                'multiple' => false,
                'required' => false,
            ))
            ->add('altoVidrio', 'number', array('label' => 'Alto Vidrio (mm)'))
            ->add('anchoVidrio', 'number', array('label' => 'Ancho Vidrio (mm)'))
            ->add('premarco')
            ->add('contramarco')
            ->add('cantidad')
            ->add('enabled')
            ->add('costo', 'number', array(
                'required' => false,
                'disabled' => true
            ))
            ->add('presupuesto', null, array(
                'required' => true,
                'disabled' => true
            ))
            ->add('save', 'submit', array(
                'label' => 'Guardar Carpinteria'
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Carpinteria::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'carpinteria';
    }


}
