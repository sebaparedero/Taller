<?php

namespace CarpinteriaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Carpinterias
 *
 * @ORM\Table(name="carpinterias")
 * @ORM\Entity(repositoryClass="CarpinteriaBundle\Repository\CarpinteriaRepository")
 */
class Carpinteria
{

    /**
     * @ORM\ManyToOne(targetEntity="PresupuestoBundle\Entity\Presupuesto", inversedBy="carpinteria")
     * @ORM\JoinColumn(name="presupuesto_id", referencedColumnName="id")
     */
    private $presupuesto;

    /**
     * @ORM\ManyToOne(targetEntity="VidrioBundle\Entity\Vidrio", inversedBy="carpinteria")
     * @ORM\JoinColumn(name="vidrio_id", referencedColumnName="id")
     */
    private $vidrio;

    /**
     * @ORM\ManyToOne(targetEntity="AberturaBundle\Entity\Abertura", inversedBy="carpinteria")
     * @ORM\JoinColumn(name="abertura_id", referencedColumnName="id")
     */
    private $abertura;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $cantidad;

    /**
     * @var int
     *
     * @ORM\Column(name="alto_vidrio", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $altoVidrio;

    /**
     * @var int
     *
     * @ORM\Column(name="ancho_vidrio", type="integer")
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\GreaterThan(
     *     value = 0
     * )
     */
    private $anchoVidrio;

    /**
     * @var bool
     *
     * @ORM\Column(name="premarco", type="boolean")
     * @Assert\Type("boolean")
     */
    private $premarco;

    /**
     * @var bool
     *
     * @ORM\Column(name="contramarco", type="boolean")
     * @Assert\Type("boolean")
     */
    private $contramarco;

    /**
     * @var float
     *
     * @ORM\Column(name="costo", type="float")
     * @Assert\Type("float")
     * @Assert\GreaterThan(
     *     value = -1
     * )
     */
    private $costo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     *
     * @return Carpinteria
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return int
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set altoVidrio
     *
     * @param integer $altoVidrio
     *
     * @return Carpinteria
     */
    public function setAltoVidrio($altoVidrio)
    {
        $this->altoVidrio = $altoVidrio;

        return $this;
    }

    /**
     * Get altoVidrio
     *
     * @return int
     */
    public function getAltoVidrio()
    {
        return $this->altoVidrio;
    }



    /**
     * Set anchoVidrio
     *
     * @param integer $anchoVidrio
     *
     * @return Carpinteria
     */
    public function setAnchoVidrio($anchoVidrio)
    {
        $this->anchoVidrio = $anchoVidrio;

        return $this;
    }

    /**
     * Get anchoVidrio
     *
     * @return int
     */
    public function getAnchoVidrio()
    {
        return $this->anchoVidrio;
    }

    /**
     * Set premarco
     *
     * @param boolean $premarco
     *
     * @return Carpinteria
     */
    public function setPremarco($premarco)
    {
        $this->premarco = $premarco;

        return $this;
    }

    /**
     * Get premarco
     *
     * @return bool
     */
    public function getPremarco()
    {
        return $this->premarco;
    }

    /**
     * Set contramarco
     *
     * @param boolean $contramarco
     *
     * @return Carpinteria
     */
    public function setContramarco($contramarco)
    {
        $this->contramarco = $contramarco;

        return $this;
    }

    /**
     * Get contramarco
     *
     * @return bool
     */
    public function getContramarco()
    {
        return $this->contramarco;
    }

    /**
     * Set vidrio
     *
     * @param \VidrioBundle\Entity\Vidrio $vidrio
     *
     * @return Carpinteria
     */
    public function setVidrio(\VidrioBundle\Entity\Vidrio $vidrio = null)
    {
        $this->vidrio = $vidrio;

        return $this;
    }

    /**
     * Get vidrio
     *
     * @return \VidrioBundle\Entity\Vidrio
     */
    public function getVidrio()
    {
        return $this->vidrio;
    }

    /**
     * Set abertura
     *
     * @param \AberturaBundle\Entity\Abertura $abertura
     *
     * @return Carpinteria
     */
    public function setAbertura(\AberturaBundle\Entity\Abertura $abertura = null)
    {
        $this->abertura = $abertura;

        return $this;
    }

    /**
     * Get abertura
     *
     * @return \AberturaBundle\Entity\Abertura
     */
    public function getAbertura()
    {
        return $this->abertura;
    }

    public function getModelo()
    {
        return $this->getAbertura()->getModelo();
    }

    public function getMarca()
    {
        return $this->getAbertura()->getMarca();
    }

    public function getImage()
    {
        return $this->getAbertura()->getModelo()->getImage();
    }

    /**
     * Set presupuesto
     *
     * @param \PresupuestoBundle\Entity\Presupuesto $presupuesto
     *
     * @return Carpinteria
     */
    public function setPresupuesto(\PresupuestoBundle\Entity\Presupuesto $presupuesto = null)
    {
        $this->presupuesto = $presupuesto;

        return $this;
    }

    /**
     * Get presupuesto
     *
     * @return \PresupuestoBundle\Entity\Presupuesto
     */
    public function getPresupuesto()
    {
        return $this->presupuesto;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Carpinteria
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    public function __construct()
    {
        $this->premarco = true;
        $this->enabled = true;
    }

//    public function __toString() {
//        $modelo = ($this->getAbertura()->getModelo()->getNombre())?:'n/a';
//        $marca  = ($this->getAbertura()->getMarca()->getNombre())?:'n/a';
//        $modeloymarca = ($modelo.', '.$marca)?:'n/a';
//        $vidrioDescripcion  = $this->getVidrio()->getDescripcion()?:'n/a';
//        $medidas = ($this->getAnchoVidrio().'x'.$this->getAltoVidrio())?:'n/a';
//        $carpinteriaCompleta = ($modeloymarca.'  |  Vidrio: '.$vidrioDescripcion.'  |  '.$medidas.'  |  Cantidad: '
//            .$this->getCantidad())?:'n/a';
//        return (string) $carpinteriaCompleta?:'n/a';
//    }

}
