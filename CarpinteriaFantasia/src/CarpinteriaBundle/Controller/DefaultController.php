<?php

namespace CarpinteriaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CarpinteriaBundle:Default:index.html.twig');
    }
}
