<?php

namespace CarpinteriaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class AjaxController extends Controller
{
    /**
     * @Route("/ajax")
     */
    public function indexAction()
    {
        return $this->render('CarpinteriaBundle:Ajax:index.html.twig', array(

        ));
    }

    /**
     * @Route("/ajax/aberturas", name="ajax_aberturas")
     * @Method({"GET"})
     */
    public function aberturasAction(Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);

            $em = $this->getDoctrine()->getManager();
            $aberturas =  $em->getRepository('AberturaBundle:Abertura')->findAll();
            $response = new JsonResponse();
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'posts' => $serializer->serialize($aberturas, 'json')
            ));
            return $response;
        }
    }

}