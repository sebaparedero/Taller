<?php

namespace CarpinteriaBundle\Controller;

use CarpinteriaBundle\Entity\Carpinteria;
use PresupuestoBundle\Form\PresupuestoType;
use PresupuestoBundle\Entity\Presupuesto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route as Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use CarpinteriaBundle\Form\CarpinteriaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

class CarpinteriaController extends Controller
{

    // ------ ACCIONES ------ //

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $carpinterias = $em->getRepository('CarpinteriaBundle:Carpinteria')->findAll();
        return $this->render('CarpinteriaBundle:Carpinteria:listar.html.twig', array('carpinterias' => $carpinterias));

    }

    public function addAction($id)
    {
        $carpinteria = new Carpinteria();
        $form = $this->createCreateForm($carpinteria);
        $em = $this->getDoctrine()->getManager();
        $vidrios = $em->getRepository('VidrioBundle:Vidrio')->findAll();
        $modelos = $this->findModelosAberturas();
        return $this->render('CarpinteriaBundle:Carpinteria:form.html.twig', array(
            'modelos' => $modelos,
            'id' => $id,
            'vidrios' => $vidrios,
            'form' => $form->createView()
        ));
    }

    private function createCreateForm(Carpinteria $entity)
    {
        $form = $this->createForm(new CarpinteriaType(), $entity, array(
            'action' => $this->generateUrl('carpinteria_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction($id, Request $request)
    {
        $carpinteria = new Carpinteria();
        $form = $this->createCreateForm($carpinteria);
        $form->handleRequest($request);
        $modelos = $this->findModelosAberturas();

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            // Asigno presupuesto
                $presupuesto = $em->getRepository('PresupuestoBundle:Presupuesto')->find($id);
                $carpinteria->setPresupuesto($presupuesto);
                $presupuesto->addCarpinteria($carpinteria);
            $em->persist($carpinteria);
            $em->persist($presupuesto);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "La carpinteria se agrego exitosamente");
            return $this->redirectToRoute('carpinteria_add', array(
                'id' => $id
            ));
        }
        $this->addFlash('mensajeError', "La carpinteria no se agrego".$form->getErrors());
        return $this->render('CarpinteriaBundle:Carpinteria:form.html.twig', array(
            'form' => $form->createView(),
            'modelos' => $modelos,
            'id' => $id
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $carpinteria = $em->getRepository('CarpinteriaBundle:Carpinteria')->find($id);
        $presupuesto = $carpinteria->getPresupuesto();
        $presupuesto->removeCarpinteria($carpinteria);
        $em->remove($carpinteria);
        $em->flush();
        return $this->redirectToRoute('presupuesto_edit', array('id' => $presupuesto->getId()));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $carpinteria = $em->getRepository('CarpinteriaBundle:Carpinteria')->find($id);
        if (!$carpinteria)
        {
            throw $this->createNotFoundException('No se encontró la carpinteria con id: '.$id);
        }
        $editForm = $this->createForm(CarpinteriaType::class, $carpinteria);
        $editForm->handleRequest($request);
        $em->persist($carpinteria);
        $em->flush();

        $vidrios = $em->getRepository('VidrioBundle:Vidrio')->findAll();
        $marca_abertura = $carpinteria->getAbertura()->getMarca()->getId();
        $modelo_abertura = $carpinteria->getAbertura()->getModelo()->getId();
        $id_presupuesto = $carpinteria->getPresupuesto()->getId();

        return $this->render('CarpinteriaBundle:Carpinteria:edit.html.twig', array(
            'form' => $editForm->createView(),
            'vidrios' => $vidrios,
            'id' => $id_presupuesto,
            'id_marca' => $marca_abertura,
            'id_modelo' => $modelo_abertura,
        ));
    }

    // --- FIN ACCIONES --- //

    // ---------------------- //

    // ------ CONSULTAS DQL ------ //

    public function findMarcasPorModelo($id)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('m.nombre, m.id')
            ->from('MarcasBundle:Marcas', 'm')
            ->innerJoin('AberturaBundle:Abertura', 'a', 'WITH', 'm.id = a.marca')
            ->where('a.modelo = :id')
            ->andWhere('a.enabled = 1')
            ->setParameter('id', $id)
            //->distinct()
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function findMarcasAberturas()
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('m')
            ->from('MarcasBundle:Marcas', 'm')
            ->innerJoin('AberturaBundle:Abertura', 'a', 'WITH', 'm.id = a.marca')
            ->where('m.enabled = 1')
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function findVidrioPorId($id_vidrio)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('v.id, v.valorM2')
            ->from('VidrioBundle:Vidrio', 'v')
            ->where('v.id = :id_vidrio')
            ->setParameter('id_vidrio', $id_vidrio)
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function findModelosAberturas()
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('m')
            ->from('ModeloBundle:Modelo', 'm')
            ->innerJoin('AberturaBundle:Abertura', 'a', 'WITH', 'm.id = a.modelo')
            ->where('a.enabled = 1')
            ->distinct()
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function findAberturaPorModeloYMarca($id_modelo, $id_marca)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('a.id, a.valorMetroCarpinteria, a.valorMetroContramarco, a.valorMetroPremarco')
            ->from('AberturaBundle:Abertura', 'a')
            ->where('a.modelo = :id_modelo')
            ->andWhere('a.marca = :id_marca')
            ->setParameter('id_modelo', $id_modelo)
            ->setParameter('id_marca', $id_marca)
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function findAberturas()
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->select('a')
            ->from('AberturaBundle:Abertura', 'a')
            ->where('a.enabled = 1')
            //->innerJoin('AberturaBundle:Abertura', 'a', 'WITH', 'm.id = a.modelo')
            //->distinct()
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    // --- FIN CONSULTAS --- //

    // ---------------------- //

    // ------ PETICIONES AJAX ------ //

    /**
     * @Route("/updateSelect")
     */
    public function updateSelectAction()
    {
        $request = $this->container->get('request');
        $id_modelo = $request->request->get('id_modelo');
        $marcas = $this->findMarcasPorModelo($id_modelo);

        return new JsonResponse($marcas);
    }

    /**
     * @Route("/showAberturaCostos")
     */
    public function showAberturaCostosAction()
    {
        $request = $this->container->get('request');
        $id_modelo = $request->request->get('id_modelo');
        $id_marca = $request->request->get('id_marca');
        $abertura = $this->findAberturaPorModeloYMarca($id_modelo, $id_marca);

        return new JsonResponse($abertura);
    }

    /**
     * @Route("/showVidrioCosto")
     */
    public function showVidrioCostoAction()
    {
        $request = $this->container->get('request');
        $id_vidrio = $request->request->get('id_vidrio');
        $vidrio = $this->findVidrioPorId($id_vidrio);

        return new JsonResponse($vidrio);
    }

    // --- FIN AJAX --- //

}