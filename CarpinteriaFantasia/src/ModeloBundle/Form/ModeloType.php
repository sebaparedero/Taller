<?php

namespace ModeloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ModeloType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class)
            #->add('imagen', 'sonata_media_type', array(
            #    'provider' => 'sonata.media.provider.image',
            #    'context'  => 'default'
            #))
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => 'Archivo de imagen',
                'allow_delete' => true,
                'download_uri' => false,
                'image_uri' => true,
                'imagine_pattern' => 'medium'
            ])
            ->add('enabled', null, array(
                'required' => false,
                'label' => 'Activado'
            ))
            ->add('save', 'submit', array('label' => 'Guardar'))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ModeloBundle\Entity\Modelo'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'modelo';
    }


}
