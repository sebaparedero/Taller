<?php

namespace ModeloBundle\Controller;

use ModeloBundle\Entity\Modelo;
use ModeloBundle\Form\ModeloType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ModeloController extends Controller
{

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $modelos = $em->getRepository('ModeloBundle:Modelo')->findAll();
        return $this->render('ModeloBundle:Modelo:listar.html.twig', array('modelos' => $modelos));

    }

    public function unlistAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entidades = $em->getRepository('ModeloBundle:Modelo')->findEliminados();
        return $this->render('ModeloBundle:Modelo:unlisted.html.twig', array(
            'modelos' => $entidades
        ));
    }

    public function addAction()
    {
        $modelo = new Modelo();
        $form = $this->createCreateForm($modelo);
        return $this->render('ModeloBundle:Modelo:form.html.twig', array(
            'form' => $form->createView(),
            'tituloForm' => 'Agregar Modelo',
        ));
    }

    private function createCreateForm(Modelo $entity)
    {
        $form = $this->createForm(new ModeloType(), $entity, array(
            'action' => $this->generateUrl('modelo_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $modelo = new Modelo();
        $form = $this->createCreateForm($modelo);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modelo);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "El modelo agrego exitosamente");
            return $this->redirectToRoute('modelo_add');
        }

        return $this->render('ModeloBundle:Modelo:form.html.twig', array('form' => $form->createView()));
    }

        public function editAction($id, Request $request)
        {
            $em = $this->getDoctrine()->getManager();
            $entidad = $em->getRepository('ModeloBundle:Modelo')->find($id);
            if (!$entidad)
            {
                throw $this->createNotFoundException('No se encontró el modelo con id: '.$id);
            }

            $editForm = $this->createForm(ModeloType::class, $entidad);
            $editForm->handleRequest($request);
            $em->persist($entidad);
            $em->flush();

            return $this->render('ModeloBundle:Modelo:form.html.twig', array(
                'form' => $editForm->createView(),
                'modelo' => $entidad,
                'tituloForm' => 'Editar Modelo',
            ));
        }

    public function activarAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ModeloBundle:Modelo')->find($id);
        $entidad->setEnabled(1);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('modelo_unlist');
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ModeloBundle:Modelo')->find($id);
        $entidad->setEnabled(0);
        $em->persist($entidad);
        $em->flush();
        return $this->redirectToRoute('modelo_listar');
    }

    public function deleteFuerzaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('ModeloBundle:Modelo')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('modelo_unlist');
    }

}