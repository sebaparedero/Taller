<?php

namespace ModeloBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Modelos
 *
 * @ORM\Table(name="modelos")
 * @ORM\Entity(repositoryClass="ModeloBundle\Repository\ModeloRepository")
 * @UniqueEntity("nombre")
 * @Vich\Uploadable
 */
class Modelo
{

    /**
     * @ORM\OneToMany(targetEntity="AberturaBundle\Entity\Abertura", mappedBy="modelo")
     */
    private $aberturas;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=80, unique=true)
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 80,
     *      minMessage = "El nombre tiene que tener al menos 2 caracteres",
     *      maxMessage = "El nombre no puede tener mas de 80 caracteres"
     * )
     */
    private $nombre;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="modelo_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     *
     * @Assert\Image(
     *     maxWidth = 1000,
     *     maxHeight = 1000,
     *     maxWidthMessage = "Imagen muy grande. Máximo permitido: 1000x1000px",
     *     maxHeightMessage = "Imagen muy grande. Máximo permitido: 1000x1000px"
     * )
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     * @Assert\Type("boolean")
     */
    private $enabled;

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Modelo
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Modelo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __construct()
    {
        $this->enabled = true;
        $this->aberturas = new ArrayCollection();
    }

    /**
     * Add abertura
     *
     * @param \AberturaBundle\Entity\Abertura $abertura
     *
     * @return Modelo
     */
    public function addAbertura(\AberturaBundle\Entity\Abertura $abertura)
    {
        $this->aberturas[] = $abertura;

        return $this;
    }

    /**
     * Remove abertura
     *
     * @param \AberturaBundle\Entity\Abertura $abertura
     */
    public function removeAbertura(\AberturaBundle\Entity\Abertura $abertura)
    {
        $this->aberturas->removeElement($abertura);
    }

    /**
     * Get aberturas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAberturas()
    {
        return $this->aberturas;
    }

    public function __toString() {
        return $this->nombre?:'n/a';
    }

#    /**
#     * Set imagen
#     *
#     * @param \Application\Sonata\MediaBundle\Entity\Media $imagen
#     *
#     * @return Modelo
#     */
#    public function setImagen(\Application\Sonata\MediaBundle\Entity\Media $imagen = null)
#    {
#        $this->imagen = $imagen;

#        return $this;
#    }

#    /**
#     * Get imagen
#     *
#     * @return \Application\Sonata\MediaBundle\Entity\Media
#     */
#    public function getImagen()
#    {
#        return $this->imagen;
#    }

    /**
     * @param string $imageName
     *
     * @return Modelo
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Modelo
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * @return integer|null
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Modelo
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Modelo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
