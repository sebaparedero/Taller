<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/** 
* @ORM\Entity
* @ORM\Table(name="fos_user") 
*/ 
class User extends BaseUser{
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToMany(targetEntity="PresupuestoBundle\Entity\Presupuesto", mappedBy="user")
     */
    private $presupuestos;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group")
     * @ORM\JoinTable(name="fos_user_user_group",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;

    /**     
    * @ORM\Id
    * @ORM\Column(type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El nombre tiene que tener al menos 2 caracteres",
     *      maxMessage = "El nombre no puede tener mas de 60 caracteres"
     * )
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=60, nullable=true)
     * @Assert\Type("string")
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "El apellido tiene que tener al menos 2 caracteres",
     *      maxMessage = "El apellido no puede tener mas de 60 caracteres"
     * )
     */
    private $apellido;

    public function __construct(){
        parent::__construct();
        $this->enabled = true;
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->presupuestos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     *
     * @return User
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Add presupuesto
     *
     * @param \PresupuestoBundle\Entity\Presupuesto $presupuesto
     *
     * @return User
     */
    public function addPresupuesto(\PresupuestoBundle\Entity\Presupuesto $presupuesto)
    {
        $this->presupuestos[] = $presupuesto;

        return $this;
    }

    /**
     * Remove presupuesto
     *
     * @param \PresupuestoBundle\Entity\Presupuesto $presupuesto
     */
    public function removePresupuesto(\PresupuestoBundle\Entity\Presupuesto $presupuesto)
    {
        $this->presupuestos->removeElement($presupuesto);
    }

    /**
     * Get presupuestos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPresupuestos()
    {
        return $this->presupuestos;
    }

    public function __toString() {
        return ($this->getNombreApellido())?:'n/a';
    }

    public function getNombreApellido()
    {
        return ($this->getNombre().' '.$this->getApellido())?:'n/a';
    }
    
}
