<?php

namespace AppBundle\Form;

use Sonata\AdminBundle\Form\Type\Filter\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'Nombre de Usuario'))
            ->add('plainPassword', null, array(
                'label' => 'Contraseña'
            ))
            ->add('nombre')
            ->add('apellido')
            ->add('email')
            ->add('roles', 'choice', [
                    'choices' => [
                        'ROLE_ADMIN' => 'Dueño',
                        'ROLE_USER' => 'Empleado'
                    ],
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
            ->add('enabled', null, array('label' => 'Si'))
            ->add('save', 'submit', array('label' => 'Guardar Usuario'))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'user';
    }


}
