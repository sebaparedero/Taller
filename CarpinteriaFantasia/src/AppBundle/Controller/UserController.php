<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:User')->findAll();
        return $this->render('AppBundle:User:listar.html.twig', array('usuarios' => $usuarios));

    }

    public function addAction()
    {
        $entidad = new User();
        $form = $this->createCreateForm($entidad);
        return $this->render('AppBundle:User:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST'
        ));

        return $form;
    }

    public function createAction(Request $request)
    {
        $entidad = new User();
        $form = $this->createCreateForm($entidad);
        $form->handleRequest($request);
        $form->getErrors(true);

        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entidad);
            $em->flush();
            $this->addFlash('mensajeConfirmacion', "El usuario se agrego exitosamente");
            return $this->redirectToRoute('user_add');
        } else {
            $this->addFlash('mensajeError', 'No se pudo agregar el usuario '.$form->getErrors());
        }

        return $this->render('AppBundle:User:form.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AppBundle:User')->find($id);
        if (!$entidad)
        {
            throw $this->createNotFoundException('No se encontró el usuario con id: '.$id);
        }

        $editForm = $this->createForm(UserType::class, $entidad);
        $editForm->handleRequest($request);
        $em->persist($entidad);
        $em->flush();

        return $this->render('AppBundle:User:form.html.twig', array(
            'form' => $editForm->createView(),
            'id' => $id
        ));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entidad = $em->getRepository('AppBundle:User')->find($id);
        $em->remove($entidad);
        $em->flush();
        return $this->redirectToRoute('user_listar');
    }


}