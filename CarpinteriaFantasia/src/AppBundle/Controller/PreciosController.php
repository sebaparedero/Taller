<?php

namespace AppBundle\Controller;


use AberturaBundle\Entity\Abertura;
//use AberturaBundle\Form\AberturaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use VidrioBundle\Entity\Vidrio;
use VidrioBundle\Form\VidrioType;

class PreciosController extends Controller
{

    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $aberturas = $em->getRepository('AberturaBundle:Abertura')->findAll();
        $vidrios = $em->getRepository('VidrioBundle:Vidrio')->findAll();
        return $this->render('AppBundle:Precios:listar.html.twig', array(
            'aberturas' => $aberturas,
            'vidrios' => $vidrios
        ));

    }

    public function aumentarAberturaVidrioAction()
    {
        $request = $this->container->get('request');
        $valor = $request->request->get('valor');
        $valor = (($valor/100) + 1);
        $this->aumentarPreciosAberturas($valor);
        $this->aumentarPreciosVidrios($valor);
        return $this->redirectToRoute('precio_listar');
    }

    // ------ AUMENTAR PRECIOS VIA DQL ------ //

    public function aumentarPrecioVidrio($valor)
    {
    	$em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->update('VidrioBundle:Vidrio', 'v')
            ->set('v.valorM2 = (v.valorM2 * :valor)')
            ->setParameter('valor', $valor)
            //->distinct()
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    public function aumentarPrecioAbertura($valor)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder
            ->update('AberturaBundle:Abertura', 'a')
            ->set('a.valorMetroCarpinteria = (a.valorMetroCarpinteria * :valor)')
            ->set('a.valorMetroContramarco = (a.valorMetroContramarco * :valor)')
            ->set('a.valorMetroPremarco = (a.valorMetroPremarco * :valor)')
            ->setParameter('valor', $valor)
            //->distinct()
        ;
        $query = $queryBuilder->getQuery();
        return $query->getResult();
    }

    // --- FIN AUMENTAR PRECIOS DQL --- //


    // ------ AUMENTAR PRECIOS VIA ENTITY MANAGER ------ //

    public function aumentarPreciosAberturas($valor)
    {
        $em = $this->getDoctrine()->getManager();
        $aberturas = $em->getRepository('AberturaBundle:Abertura')->findAll();
        foreach($aberturas as $abertura) {
            $valorCarpinteria = $abertura->getValorMetroCarpinteria() * $valor;
            $valorContramarco = $abertura->getValorMetroContramarco() * $valor;
            $valorPremarco = $abertura->getValorMetroPremarco() * $valor;
            $abertura->setValorMetroCarpinteria($valorCarpinteria);
            $abertura->setValorMetroContramarco($valorContramarco);
            $abertura->setValorMetroPremarco($valorPremarco);
            $em->persist($abertura);
        }
        $em->flush();
    }

    public function aumentarPreciosVidrios($valor)
    {
        $em = $this->getDoctrine()->getManager();
        $vidrios = $em->getRepository('VidrioBundle:Vidrio')->findAll();
        foreach($vidrios as $vidrio) {
            $valorM2Nuevo = $vidrio->getValorM2() * $valor;
            $vidrio->setValorM2($valorM2Nuevo);
            $em->persist($vidrio);
        }
        $em->flush();
    }


    // --- FIN AUMENTAR PRECIOS ENTITY MANAGER --- //




//    public function addAction()
//    {
//        $abertura = new Abertura();
//        $form = $this->createCreateForm($abertura);
//        return $this->render('AberturaBundle:Abertura:add.html.twig', array('form' => $form->createView()));
//    }

//    private function createCreateForm(Modelo $entity)
//    {
//        $form = $this->createForm(new AberturaType(), $entity, array(
//            'action' => $this->generateUrl('abertura_create'),
//            'method' => 'POST'
//        ));
//
//        return $form;
//    }

//    public function createAction(Request $request)
//    {
//        $abertura = new Abertura();
//        $form = $this->createCreateForm($abertura);
//        $form->handleRequest($request);

//        if($form->isValid())
//        {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($modelo);
//            $em->flush();
//            $this->addFlash('mensajeConfirmacion', "El modelo agrego exitosamente");
//            return $this->redirectToRoute('abertura_add');
//        }

//        return $this->render('AberturaBundle:Abertura:add.html.twig', array('form' => $form->createView()));
//    }


}