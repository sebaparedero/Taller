<?php

namespace AppBundle\DataFixtures\ORM;

use MarcasBundle\Entity\Marcas;
use ModeloBundle\Entity\Modelo;
use VidrioBundle\Entity\Vidrio;
use ClienteBundle\Entity\Cliente;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 20; $i++) {
            $marca = $this->crearMarca($i);
            $modelo = $this->crearModelo($i);
            $vidrio = $this->crearVidrio($i);
            $cliente = $this->crearCliente($i);
            $manager->persist($marca);
            $manager->persist($modelo);
            $manager->persist($vidrio);
            $manager->persist($cliente);
        }

        $manager->flush();
    }

    public function crearModelo($numero)
    {
        $modelo = new Modelo();
        $modelo->setNombre('modelo ' . $numero);
        return $modelo;
    }

    public function crearMarca($numero)
    {
        $marca = new Marcas();
        $marca->setNombre('marca ' . $numero);
        return $marca;
    }

    public function crearCliente($numero)
    {
        $cliente = new Cliente();
        $cliente->setNombre('cliente ' . $numero);
        $cliente->setApellido('apellido ' . $numero);
        $cliente->setDomicilio('domicilio ' . $numero);
        $cliente->setTelefono(2983527771);
        $cliente->setEmail('cliente'.$numero.'@hotmail.com');
        return $cliente;
    }

    public function crearVidrio($numero)
    {
        $vidrio = new Vidrio();
        $vidrio->setDescripcion('vidrio ' . $numero);
        $vidrio->setValorM2(mt_rand(50, 1000));
        $vidrio->setEspesor(mt_rand(0, 20));
        return $vidrio;
    }

}