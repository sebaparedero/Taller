<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as ControllerContainer;
use AppBundle\Entity\User;
use AppBundle\Entity\Group;

class LoadUserData extends ControllerContainer implements FixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $role_user = ['ROLE_USER'];
        $role_admin = ['ROLE_USER', 'ROLE_ADMIN'];
        $grupoEmpleados = $this->crearGrupo('Empleados', $role_user);
        $grupoDuenos = $this->crearGrupo('Dueños', $role_admin);
        $userAdmin = $this->crearUser('admin', 'admin');
        $userAdmin->setRoles($role_admin);
        $manager->persist($grupoEmpleados);
        $manager->persist($grupoDuenos);
        $manager->persist($userAdmin);
        $manager->flush();
    }

    public function crearGrupo($nombre, $roles)
    {
        $grupo = new Group();
        $grupo->setName($nombre);
        $grupo->setRoles($roles);
        return $grupo;
    }

    public function crearUser($nombre, $password)
    {
        $userAdmin = new User();
        $userAdmin->setUsername($nombre);
        $userAdmin->setNombre($nombre);
        $userAdmin->setApellido($nombre);
        $encoder = $this->container->get('security.password_encoder');
        $userAdmin->setPassword($encoder->encodePassword($userAdmin, $password));
        $userAdmin->setEmail($nombre.'@hotmail.com');
        return $userAdmin;
    }

}
