<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class UserAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Usuario', array('class' => 'col-md-4'))
                ->add('username', 'text', array('label' => 'Nombre de Usuario'))
                ->add('plainPassword', 'text', array(
                    'label' => 'Contraseña'
                ))
                ->add('email', 'text')
            ->end()
            ->with('Perfil', array('class' => 'col-md-4'))
                ->add('nombre', 'text')
                ->add('apellido', 'text')
            ->end()
            // >>> Para corregir, todavia no asigna bien los roles !!!
            ->with('Seguridad', array('class' => 'col-md-4'))
                ->add('groups', 'sonata_type_model', array(
                    'class' => 'AppBundle\Entity\Group',
                    'property' => 'name',
                    'expanded' => true,
                    'multiple' => true,
                    'label'   => 'Tipo de Usuario'
                ))
                ->add('enabled',
                    'choice', array(
                        'label' => 'Activado',
                        'choices' => array(
                            'true' => 'Si',
                            'false' => 'No'
                        ))
                )
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, array('label' => 'Nombre de Usuario'))
            ->add('email')
            ->add('nombre')
            ->add('apellido')
            ->add('groups', null, array('label' => 'Rol'), 'entity', array(
                'class'    => 'AppBundle\Entity\Group',
                'property' => 'name',
            ))
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, array('label' => 'Nombre de Usuario'))
            ->addIdentifier('email')
            ->add('nombre')
            ->add('apellido')
            ->add('groups', null, array(
                'label' => 'Rol',
                'editable' => false
            ))
            ->add('enabled', null, array(
                'editable' => true,
                'label' => 'Activado'
            ))
        ;
    }
}