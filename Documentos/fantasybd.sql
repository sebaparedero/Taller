-- MySQL Workbench Forward Engineering

-- -----------------------------------------------------
-- Tabla informacion_personal
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `informacion_personal` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(20) NULL,
  `domicilio` VARCHAR(45) NULL,
  `email` VARCHAR(90) NULL, 
	PRIMARY KEY (`ID`)
);


-- -----------------------------------------------------
-- Tabla user
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `informacion_personal_ID` INT NOT NULL,
  `username` varchar(16) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipo` VARCHAR(45) NOT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `fk_user_persona1`
		FOREIGN KEY (`informacion_personal_ID`) REFERENCES informacion_personal(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Tabla tipo_vidrio
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tipo_vidrio` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(50) NOT NULL,
  `valor_m2` FLOAT NOT NULL,
  `espesor` INT NOT NULL,
PRIMARY KEY (`ID`)
);


-- -----------------------------------------------------
-- Tabla marcas_aberturas
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `marcas_aberturas` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NOT NULL,
	PRIMARY KEY (`ID`)
);


-- -----------------------------------------------------
-- Tabla modelos_aberturas
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `modelos_aberturas` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(45) NOT NULL,
  `imagen` BLOB NULL,
	PRIMARY KEY (`ID`)
);


-- -----------------------------------------------------
-- Tabla tipo_abertura
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tipo_abertura` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `marca_ID` INT NOT NULL,
  `modelo_ID` INT NOT NULL,
  `valor_metro_carpinteria` FLOAT NOT NULL,
  `valor_metro_premarco` FLOAT NOT NULL,
  `valor_metro_contramarco` FLOAT NOT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `fk_abertura_marca_has_abertura_modelo_abertura_marca1`
		FOREIGN KEY (`marca_ID`) REFERENCES marcas_aberturas(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_abertura_marca_has_abertura_modelo_abertura_modelo1`
		FOREIGN KEY (`modelo_ID`) REFERENCES modelos_aberturas(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Tabla cliente
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cliente` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `informacion_personal_ID` INT NOT NULL,
  `observaciones` TEXT(20000) NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `fk_cliente_informacion_personal1`
		FOREIGN KEY (`informacion_personal_ID`) REFERENCES informacion_personal(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Tabla presupuestos
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `presupuestos` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `user_ID` INT NOT NULL,
  `cliente_ID` INT NOT NULL,
  `fecha` DATE NOT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `fk_presupuestos_user1`
		FOREIGN KEY (`user_ID`) REFERENCES user(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_presupuestos_cliente1`
		FOREIGN KEY (`cliente_ID`) REFERENCES cliente(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Tabla carpinterias
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carpinterias` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `presupuesto_ID` INT NOT NULL,
  `tipo_vidrio_ID` INT NOT NULL,
  `tipo_abertura_ID` INT NOT NULL,
  `cantidad` INT NOT NULL,
  `alto_vidrio` INT NOT NULL,
  `ancho_vidrio` INT NOT NULL,
  `premarco` BOOLEAN NOT NULL,
  `contramarco` BOOLEAN NOT NULL,
	PRIMARY KEY (`ID`),
	CONSTRAINT `fk_carpinterias_presupuestos1`
		FOREIGN KEY (`presupuesto_ID`) REFERENCES presupuestos(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_carpinterias_tipo_vidrio1`
		FOREIGN KEY (`tipo_vidrio_ID`) REFERENCES tipo_vidrio(`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_carpinterias_tipo_abertura1`
		FOREIGN KEY (`tipo_abertura_ID`) REFERENCES tipo_abertura(`ID`) ON DELETE CASCADE ON UPDATE CASCADE
);